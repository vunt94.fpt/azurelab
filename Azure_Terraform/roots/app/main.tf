terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.49.0"
    }
  }
  backend "azurerm" {
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

data "terraform_remote_state" "vn" {
  backend = "azurerm"

  config = {
    resource_group_name  = var.backend_resource_group_name
    storage_account_name = var.backend_storage_account_name
    container_name       = var.backend_container_name
    key                  = var.backend_state_file
  }
}

locals {
  network_interface_id = data.terraform_remote_state.vn.outputs.network_interface_id
  public_ip_address_id_for_vm = data.terraform_remote_state.vn.outputs.public_ip_address_for_vm_id
  public_ip_address_id_for_lb = data.terraform_remote_state.vn.outputs.public_ip_address_for_lb_id
  
}

module "app_service" {
  source                = "../../modules/app_service"
  app_service_plan_name = var.app_service_plan_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  sku_tier              = var.sku_tier
  sku_size              = var.sku_size
  app_service_name      = var.app_service_name
  
}

module "load_balancer" {
  source               = "../../modules/load_balancer"
  lb_name              = var.loadbalancer_name
  location             = var.location
  resource_group_name  = var.resource_group_name
  sku                  = var.sku_lb
  fname                = var.fe_ip_config_name
  public_ip_address_id = local.public_ip_address_id_for_lb
  bap_name             = var.backend_address_pool_name
  network_interface_id = local.network_interface_id
  ip_config_name       = var.ip_config_name
  lb_probe_name        = var.lb_probe_name
  port_number          = var.port_number
  protocol             = var.protocol
  lb_rule_name         = var.lb_rule_name
  fe_port              = var.fe_port
  be_port              = var.be_port
}

module "vm" {
  source               = "../../modules/vm"
  username             = var.username
  public_key           = var.public_key
  # as_name              = var.as_name
  location             = var.location
  rg_name              = var.resource_group_name
  vm_name              = var.vm_name
  size                 = var.size
  admin_username       = var.admin_username
  ni_id                = local.network_interface_id
  caching              = var.caching
  storage_account_type = var.storage_account_type
  publisher            = var.publisher
  offer                = var.offer
  sku                  = var.sku
  si_version           = var.si_version
}