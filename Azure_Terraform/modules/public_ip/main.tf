resource "azurerm_public_ip" "public_ip_for_VM" {
  name                = var.public_ip_for_vm_name
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = var.allocation_method
  sku = var.sku
}

resource "azurerm_public_ip" "public_ip_for_LB" {
  name                = var.public_ip_for_lb_name
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = var.allocation_method
  sku = var.sku
}