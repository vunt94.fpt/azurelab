# variable "as_name" {
#   type = string
# }

variable "vm_name" {
  type = string
}

variable "location" {
  type = string
}

variable "rg_name" {
  type = string
}

variable "size" {
  type = string
}

variable "admin_username" {
  type = string
}

variable "ni_id" {
  type = string
}

variable "caching" {
  type = string
}

variable "storage_account_type" {
  type = string
}

variable "publisher" {
  type = string
}

variable "offer" {
  type = string
}

variable "sku" {
  type = string
}

variable "si_version" {
  type = string
}

variable "username" {
  type = string
}

variable "public_key" {
  type = string
}