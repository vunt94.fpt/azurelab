# resource "azurerm_availability_set" "as" {
#   name                = var.as_name
#   location            = var.location
#   resource_group_name = var.rg_name
# }

resource "azurerm_linux_virtual_machine" "vm" {
  name                = var.vm_name
  resource_group_name = var.rg_name
  location            = var.location
  size                = var.size # "Standard_F2"
  admin_username      = var.admin_username #"adminuser"
  network_interface_ids = [
    var.ni_id
  ]

  os_disk {
    caching              = var.caching # "ReadWrite"
    storage_account_type = var.storage_account_type # "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = var.public_key
  }

  source_image_reference {
    publisher = var.publisher
    offer = var.offer
    sku = var.sku
    version = var.si_version
  }
}