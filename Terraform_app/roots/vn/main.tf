terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.49.0"
    }
  }
  backend "azurerm" {
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

module "vn" {
  source              = "../../modules/vn"
  location            = var.location
  resource_group_name = var.resource_group_name
  vn_name             = var.vn_name
  address_space       = var.address_space
  subnet_name         = var.subnet_name
  address_prefixes    = var.address_prefixes
}

module "public_ip" {
  source              = "../../modules/public_ip"
  public_ip_for_vm_name      = var.public_ip_for_vm_name
  public_ip_for_lb_name = var.public_ip_for_lb_name
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = var.allocation_method
  sku                 = var.sku
}

module "network_interface" {
  source                        = "../../modules/network_interface"
  ni_name                       = var.network_interface_name
  location                      = var.location
  resource_group_name           = var.resource_group_name
  ip_config_name                = var.ip_config_name
  subnet_id                     = module.vn.subnet_id
  private_ip_address_allocation = var.private_ip_address_allocation
  public_ip_address_id          = module.public_ip.public_ip_for_vm_id
  network_security_group_id = module.security_group.network_security_group_id
}

module "security_group" {
  source              = "../../modules/security_group"
  security_group_name = var.security_group_name
  location            = var.location
  resource_group_name = var.resource_group_name
}

