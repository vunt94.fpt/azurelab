output "location" {
  value = module.resource_group.location
}

output "resource_group_name" {
  value = module.resource_group.rg_name
}
